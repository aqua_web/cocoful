<?php
$label = "コラム・連載";

if ($wp_query->get('column_category')) {
	/**
	 * 第２階層
	 */
	$term = get_term_by('slug', $wp_query->get('column_category'), 'column_category');
	$parent = $label;
	$label = sprintf("ママ向け%s一覧", $term->name);
	$slug = $term->slug;
	$h1 = $term->name . '一覧ページ';
	$footerAddText = sprintf("外出の参考にして欲しい%s一覧ページ。", $term->name);
} else {
	/**
	 * 第１階層
	 */
	$post_type = 'column';
	$category_term = 'column_category';
	$h1 = "コラム・連載一覧ページ";
	$footerAddText = "外出の参考にして欲しいコラム・連載一覧ページ。";
	// 下層カテゴリ取得
	$sub_cats = get_terms('column_category', array('hide_empty' => 0, 'parent' => 0, 'orderby' => 'order'));
}
get_header();
?>
	<!-- content_area -->
	<div id="content_area">
		<!-- content -->
		<div id="content">
<?php get_sidebar();?>
			<!-- main -->
			<div id="main">
<?php
if ($wp_query->get('column_category')) {
	include "include_archive_2.php";
} else {
	include "include_archive_1.php";
}
?>
			</div>
			<!-- /main -->
		</div>
		<!-- /content -->
	</div>
	<!-- /content_area -->
</div>
<!-- /wrap -->
<?php get_footer();?>
