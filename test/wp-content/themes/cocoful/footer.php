<?php global $footerAddText; ?>
<!-- footer_area -->
<div id="footer_area">
	<p class="pagetop"><a href="#header" class="alpha"><img src="<?php echo bloginfo('siteurl');?>/common/images/btn_pagetop.gif" width="115" height="31" alt="PAGE TOP" /></a></p>
	<!-- footer -->
	<div id="footer">
		<div id="fnav" class="px11">
			<ul>
				<li><a href="<?php echo bloginfo('siteurl');?>/">トップページ</a></li>
				<li><a href="index.html">おでかけ</a></li>
				<li><a href="<?php echo bloginfo('siteurl');?>/station/index.html">駅構内情報</a></li>
				<li><a href="<?php echo bloginfo('siteurl');?>/event/index.html">イベント・企画</a></li>
				<li><a href="<?php echo bloginfo('siteurl');?>/special/index.html">特集</a></li>
			</ul>
			<dl>
				<dt><a href="<?php echo bloginfo('siteurl');?>/column/index.html">コラム・連載</a></dt>
				<dd>（<a href="<?php echo bloginfo('siteurl');?>/column/culture/index.html">カルチャー</a></dd>
				<dd>｜<a href="<?php echo bloginfo('siteurl');?>/column/life/index.html">ライフ</a></dd>
				<dd>｜<a href="<?php echo bloginfo('siteurl');?>/column/food/index.html">フード</a></dd>
				<dd>｜<a href="<?php echo bloginfo('siteurl');?>/column/season/index.html">シーズン</a>）</dd>
			</dl>
			<dl>
				<dt><a href="<?php echo bloginfo('siteurl');?>/working/index.html">まなぶ・はたらく</a></dt>
				<dd>（<a href="<?php echo bloginfo('siteurl');?>/working/job/index.html">求人情報</a></dd>
				<dd>｜<a href="<?php echo bloginfo('siteurl');?>/working/school/index.html">スクール情報</a>）</dd>
			</dl>
			<dl>
				<dt><a href="<?php echo bloginfo('siteurl');?>/about/index.html">ココフルとは</a></dt>
				<dt><a href="<?php echo bloginfo('siteurl');?>/company/index.html">会社概要</a></dt>
				<dd>（<a href="<?php echo bloginfo('siteurl');?>/company/service/index.html">サービス紹介</a></dd>
				<dd>｜<a href="<?php echo bloginfo('siteurl');?>/company/service/service1.html">サービス事例</a>）</dd>
			</dl>
			<ul>
				<li><a href="<?php echo bloginfo('siteurl');?>/kiyaku/index.html">利用規約</a></li>
				<li><a href="<?php echo bloginfo('siteurl');?>/privacy/index.html">プライバシーポリシー</a></li>
				<li><a href="https://s360.jp/form/32166-13/" target="_blank">お問い合わせ</a></li>
				<li><a href="<?php echo bloginfo('siteurl');?>/sitemap/index.html">サイトマップ</a></li>
			</ul>
		</div>
		<p id="footer_logo"><img src="<?php echo bloginfo('siteurl');?>/common/images/logo_02.gif" width="209" height="46" alt="Cocoful ココフル とは" /></p>
		<p id="footer_seo" class="px11">ココフルは妊娠・出産・育児世代が楽しめるイベントや情報をお届けしています。<br />
        首都圏、東京、神奈川、千葉、埼玉を中心に全国へ。ママ・パパ同士のつながりやイベント等のつながりをお手伝い・支援しています。<br />
        ママが一歩外に出ることを応援し、子育て世代ならではの感性をいかした商品開発・座談会・イベントやプロジェクトを企画。<br />
        ママとママ、ママと企業をつないでいきます。おでかけ以外にも、学ぶ・働くなどの情報や、自宅で参加できる企画、プレゼント情報、季節に合わせたコラムなど情報満載です。駅の情報も随時更新。エレベーター・エスカレータ－の他、親子でお出かけする気になる点をコメントで紹介。<?php echo esc_html($footerAddText);?></p>
	</div>
	<!-- /footer -->
	<div class="copyright">
		<p class="px10">Copyright&copy; HITOMINA, Inc. All rights reserved.</p>
	</div>
</div>
<!-- /footer_area -->
<?php wp_footer();?>

<!--↓ClickTale end tagここから↓-->  
<!-- ClickTale Bottom part -->

<script type='text/javascript'>
// The ClickTale Balkan Tracking Code may be programmatically customized using hooks:
// 
//   function ClickTalePreRecordingHook() { /* place your customized code here */  }
//
// For details about ClickTale hooks, please consult the wiki page http://wiki.clicktale.com/Article/Customizing_code_version_2

document.write(unescape("%3Cscript%20src='"+
(document.location.protocol=='https:'?
"https://cdnssl.clicktale.net/www07/ptc/b77f6539-ff5a-49c0-a0fb-06813b9baff7.js":
"http://cdn.clicktale.net/www07/ptc/b77f6539-ff5a-49c0-a0fb-06813b9baff7.js")+"'%20type='text/javascript'%3E%3C/script%3E"));
</script>

<!-- ClickTale end of Bottom part -->
<!--↑ClickTale end tagここまで↑-->

</body>
</html>