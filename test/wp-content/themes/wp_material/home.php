<?php get_header() ?>

<?php get_template_part("ad_728") ?>





				<div class="section_news">
					<div id="tabvanilla" class="menu1box box">
						<div id="main1" class="tabdiv">
<?php 
$icon = array(
	'outing',
	'event',
	'column',
	'working',
	'information'
);
$label = array(
	'outing' => 'おでかけ',
	'event' => 'イベント・企画',
	'column' => 'コラム・連載',
	'working' => 'まなぶ・はたらく',
	'information' => 'お知らせ'
);
?>
							<p class="news_tit"><img src="images/tit_news_01.gif" width="72" height="18" alt="新着情報" /></p>
<?php $posts = get_posts(array('post_type' => array('outing', 'event', 'column', 'working', 'information'), 'posts_per_page' => 6));?>
<?php if (count($posts)): foreach($posts as $p):?>
							<dl>
								<dt><?php echo get_the_time('Y/m/d', $p);?></dt>
								<dd class="news_img"><img src="images/txt_news_0<?php echo array_search(get_post_type($p), $icon) + 1;?>.gif" width="64" height="16" alt="<?php echo $label[get_post_type()];?>" /></dd>
								<dd class="news_txt"><a href="<?php echo get_permalink($p->ID);?>"><?php echo get_the_title($p);?></a></dd>
							</dl>
<?php endforeach; endif;?>
						</div>
<?php foreach ($icon as $ct => $posttype):?>
						<div id="main<?php echo $ct + 2;?>" class="tabdiv">
							<p class="news_tit"><img src="images/tit_news_0<?php echo $ct + 2;?>.gif" alt="<?php $label[$posttype];?>" /></p>
<?php wp_reset_query(); $posts = get_posts(array('post_type' => $posttype, 'posts_per_page' => 6));?>
<?php if (count($posts)): foreach($posts as $p):?>
							<dl>
								<dt><?php echo get_the_time('Y/m/d', $p);?></dt>
								<dd class="news_img"><img src="images/txt_news_0<?php echo $ct + 1;?>.gif" width="64" height="16" alt="<?php echo $label[get_post_type()];?>" /></dd>
								<dd class="news_txt"><a href="<?php echo get_permalink($p->ID);?>"><?php echo get_the_title($p);?></a></dd>
							</dl>
<?php endforeach; endif;?>
							</div>
<?php endforeach;?>
					</div>
				</div>






<div class="box big-box">

<h2 class="box-header main-color-font"><span class="lsf">star </span>New post</h2>

<?php get_template_part("loop") ?>

</div><!-- .big-box -->

<?php get_template_part("cat_lists") ?>

<?php get_sidebar() ?>
<?php get_footer() ?>