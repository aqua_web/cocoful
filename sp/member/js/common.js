// スクロール付きで、ウィンドを開く
function winop_scroll(page){
	window.open(page,"openwin","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no")
}


// Verisignマーク用のスクリプトを出力する
function WriteVerisign()
{
	var hostname = "www.grj.jp";
	if(location.host.indexOf("localhost") != -1)
		hostname="localhost";
	if(location.host.indexOf("C:") != -1)
		hostname="localhost";
	if(location.host.indexOf("D:") != -1)
		hostname="localhost";
	if(location.host.indexOf("grj-fsv02") != -1)
		hostname="localhost";
	else if(location.host.indexOf("yse.in") != -1)
		hostname="yse.in";
	document.write("<script type=\"text/javascript\" src=\"https://seal.verisign.com/getseal?host_name=" + hostname + "&size=S&use_flash=YES&use_transparent=YES&lang=ja\"></script>");
}

// プラン促しページ　画像差し替え用
function SetImage(id, imagePath)
{
	var imgObj = document.getElementById(id);
	if(imgObj != null && imagePath != "")
		imgObj.src = imagePath;
}

// コピーライトを出力する
function WriteCopyRight()
{
	dt = new Date();
	yy = dt.getFullYear();
	var copyRight = "Copyright &copy; " + yy + " Guthy-Renker Japan Co., Ltd. All rights reserved."; 
	document.write(copyRight);
}
