



$(function(){
	
	function smartRollover() {
	if(document.getElementsByTagName) {
		var images = document.getElementsByTagName("img");

		for(var i=0; i < images.length; i++) {
			if(images[i].getAttribute("src").match("_off."))
			{
				images[i].onmouseover = function() {
					this.setAttribute("src", this.getAttribute("src").replace("_off.", "_on."));
				}
				images[i].onmouseout = function() {
					this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));
				}
			}
		}
	}
}

if(window.addEventListener) {
	window.addEventListener("load", smartRollover, false);
}
else if(window.attachEvent) {
	window.attachEvent("onload", smartRollover);
}


    $(".pagetop a").click(function() {
        $("html, body").animate({
      	  scrollTop: 0
        }, 600);
      return false;
		});
	
});



$(function(){
	//ROLL OVER
	$(".section_member a img").hover(
		function(){
			$(this).fadeTo(200, 0.8);
		},
		function(){
			$(this).fadeTo(200, 1.0);
		}
	);
	
	
	//PAGE SCROL
	$('a[href*=#],area[href*=#]').click(function() {
		if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var $target = $(this.hash);
			$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
			if($target.length) {
				var targetOffset = $target.offset().top;
				$('html,body').animate({scrollTop: targetOffset}, 300);
				return false;
			}
		}
	});
});