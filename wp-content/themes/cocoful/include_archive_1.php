<?php
/**
 * 1階層目のテンプレート
 */
?>
				<ul id="pnav" class="px11">
					<li><a href="<?php echo bloginfo('siteurl');?>/">HOME</a></li>
					<li>&gt; <?php echo esc_html($label);?></li>
				</ul>
				<div class="section_catalog">
					<h2 class="tit01"><img src="<?php echo bloginfo('siteurl');?>/images/headers/h2_<?php echo $post_type;?>_top_01.gif" alt="<?php echo esc_html($label);?>" /></h2>
<?php foreach ($sub_cats as $ct => $cat):?>
					<div class="section_recommend pt15">
						<p class="recommend_tit"><img src="<?php echo bloginfo('siteurl');?>/images/headers/tit_<?php echo $cat->slug;?>_01.gif" alt="<?php echo $cat->name;?>" /></p>
<?php $posts = get_posts(array('post_type' => $post_type, 'posts_per_page' => 4, $category_term => $cat->slug));?>
<?php if (count($posts)): ?>
						<ul class="mb5">
<?php foreach ($posts as $ct => $p):?>
							<li class="heightLine<?php if ($ct % 4 == 3) { echo ' mr0';}?>">
								<p class="recommend_pic"><a href="<?php echo get_permalink($p->ID);?>" class="alpha"><?php echo get_the_post_thumbnail($p->ID, array(145, 116));?></a></p>
								<p class="recommend_date px11"><?php echo get_the_time('Y.m.d', $p);?></p>
								<p class="recommend_txt"><a href="<?php echo get_permalink($p->ID);?>"><?php echo get_the_title($p);?></a></p>
							</li>
<?php endforeach; ?>
						</ul>
<?php endif; ?>
						<p class="bottom_links"><a href="<?php echo str_replace("/" . $category_term . "/", "/", get_term_link($cat->slug, $category_term));?>/index.html" class="fb"><?php echo $cat->name;?>記事一覧へ</a></p>
					</div>
<?php endforeach;?>
				</div>
