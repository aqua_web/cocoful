<?php
global $h1;
?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title><?php wp_title(' | ', true, 'right');?>子連れでも安心しておでかけできる情報満載ポータルサイト　ココフル</title>
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('siteurl');?>/common/css/reset.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('siteurl');?>/common/css/fonts.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('siteurl');?>/common/css/module.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('siteurl');?>/common/css/layout.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('siteurl');?>/common/css/contents.css" media="all" />

<script type="text/javascript" src="<?php echo bloginfo('siteurl');?>/common/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('siteurl');?>/common/js/heightLine.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('siteurl');?>/common/js/common.js"></script>
<?php if  (is_front_page()):?>
<script type="text/javascript" src="common/js/jquery.nivo.slider.js"></script>
<link rel="stylesheet" href="common/css/nivo-slider.css" type="text/css" media="screen" />
<script type="text/javascript">
  $(window).load(function() {
      $('#slider').nivoSlider({
      });
  });
</script>

<script type="text/javascript" src="common/js/jquery-ui-personalized-1.5.2.packed.js"></script>
<script type="text/javascript" src="common/js/sprinkle.js"></script>

<!-- ↓GA設定ここから↓ -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47207623-1', 'auto', {'allowLinker': true});
  ga('require', 'linker');
  ga('linker:autoLink', ['s360.jp'] );
  ga('send', 'pageview');

</script>
<!-- ↑GA設定ここまで↑ -->

<!--↓synergy tracking tagここから↓-->
<script type="text/javascript">
  <!--
  var __s360_tdatas = __s360_tdatas || [];
  (function() {
    __s360_tdatas.push(['track', 'MCwzMjE2NixTQw%3D%3D', null, null]);
    var code = document.createElement('script');
    code.type = 'text/javascript';
    code.async = true;
    code.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 's360.jp/t/t.js';
    var script = document.getElementsByTagName('script')[0];
    script.parentNode.insertBefore(code, script);
  })();
  // -->
</script>
<!--↑synergy tracking tagここまで↑-->


<?php endif;?>
<?php wp_head();?>
</head>
<body>
<!--↓ClickTale start tagここから↓-->
<!-- ClickTale Top part -->
<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->
<!--↑ClickTale start tagここまで↑-->

<!-- wrap -->
<div id="wrap">
	<!-- header -->
	<div id="header">
		<h1 class="px11">子連れでも安心しておでかけできる情報満載ママ向けポータルサイト　ココフル<?php echo $h1 ? "の" . esc_html($h1) : "";?></h1>
		<p id="logo"><a href="<?php echo bloginfo('siteurl');?>/"><img src="<?php echo bloginfo('siteurl');?>/common/images/logo_01.gif" width="233" height="63" alt="Cocoful ココフル" /></a></p>
		<p id="facebook"><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fcocoful.jp%2F&amp;width=70&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=20" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:70px; height:20px;" allowTransparency="true"></iframe></p>
		<p id="twitter"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://cocoful.jp/" data-lang="ja" data-count="none">ツイート</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></p>
		<ul id="header_nav" class="px11">
			<li><a href="<?php echo bloginfo('siteurl');?>/company/index.html">会社概要</a></li>
			<li><a href="<?php echo bloginfo('siteurl');?>/sitemap/index.html">サイトマップ</a></li>
		</ul>
<!--
<p id="header_bnr"><a href="http://www.aeoncinema.com/adgf2014/" class="alpha" target="_blank"><img src="<?php echo bloginfo('siteurl');?>/common/images/bnr_header_02.jpg" width="468" height="60" alt="AEON DREAM GATE FES" /></a></p>
-->
<!--<p id="header_bnr"><a href="http://www.daichi.or.jp/ad/lpo-t?xadid=hito" class="alpha" target="_blank"><img src="<?php echo bloginfo('siteurl');?>/common/images/bnr_header_01.jpg" width="468" height="60" alt="大地を守る会" /></a></p>-->
<p id="header_bnr">
			<script type="text/JavaScript" src="<?php echo bloginfo('siteurl');?>/common/js/banner_header.js"></script>
            <script type="text/JavaScript">
            random_banner();
            </script>
        </p>

		<ul id="nav">
<?php if (is_front_page()):?>
			<li><a href="<?php echo bloginfo('siteurl');?>/"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav01_on.jpg" width="159" height="35" alt="HOME" /></a></li>
<?php else:?>
			<li><a href="<?php echo bloginfo('siteurl');?>/"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav01_off.jpg" width="159" height="35" alt="HOME" /></a></li>
<?php endif;?>
<?php if ($post_type == 'outing'):?>
			<li><a href="<?php echo bloginfo('siteurl');?>/outing/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav02_on.jpg" width="156" height="35" alt="おでかけ" /></a></li>
<?php else:?>
			<li><a href="<?php echo bloginfo('siteurl');?>/outing/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav02_off.jpg" width="156" height="35" alt="おでかけ" /></a></li>
<?php endif;?>
<?php if ($post_type == 'event'):?>
			<li><a href="<?php echo bloginfo('siteurl');?>/event/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav03_on.jpg" width="157" height="35" alt="イベント・企画" /></a></li>
<?php else:?>
			<li><a href="<?php echo bloginfo('siteurl');?>/event/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav03_off.jpg" width="157" height="35" alt="イベント・企画" /></a></li>
<?php endif;?>
<?php if ($post_type == 'column'):?>
			<li><a href="<?php echo bloginfo('siteurl');?>/column/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav04_on.jpg" width="157" height="35" alt="コラム・連載" /></a></li>
<?php else:?>
			<li><a href="<?php echo bloginfo('siteurl');?>/column/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav04_off.jpg" width="157" height="35" alt="コラム・連載" /></a></li>
<?php endif;?>
<?php if ($post_type == 'working'):?>
			<li><a href="<?php echo bloginfo('siteurl');?>/working/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav05_on.jpg" width="157" height="35" alt="まなぶ・はたらく" /></a></li>
<?php else:?>
			<li><a href="<?php echo bloginfo('siteurl');?>/working/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav05_off.jpg" width="157" height="35" alt="まなぶ・はたらく" /></a></li>
<?php endif;?>
			<li><a href="https://s360.jp/form/32166-13/" target="_blank"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav06_off.jpg" width="154" height="35" alt="お問い合わせ" /></a></li>
		</ul>
	</div>
	<!-- /header -->
