<?php
$post_type = $wp_query->query_vars['post_type'];
$noPic = false;
switch ($post_type) {
	case "outing":
		$h1 = "お出かけ情報一覧ページ";
		$footerAddText = "外出の参考にして欲しいお出かけ情報一覧ページ。";
		break;
	case "column":
		$h1 = "コラム・連載一覧ページ";
		$footerAddText = "外出の参考にして欲しいコラム・連載一覧ページ。";
		break;
	case "event":
		$h1 = "イベント・企画情報一覧ページ";
		$footerAddText = "外出の参考にして欲しいイベント・企画情報一覧ページ。";
		break;
	case "working":
		$h1 = "資格・はたらく一覧ページ";
		$footerAddText = "外出の参考にして欲しい資格・はたらく一覧ページ。";
		break;
	case "information":
		$h1 = "お知らせ一覧ページ";
		$footerAddText = "外出の参考にして欲しいお知らせ一覧ページ。";
		$noPic = true;
		break;
	case "special":
		$h1 = "特集ページ";
		$footerAddText = "外出の参考にして欲しい特集一覧ページ。";
		$noPic = true;
		break;
}
$post_type_object = get_post_type_object($post_type);
$label = $post_type_object->labels->name;
get_header();
?>
	<!-- content_area -->
	<div id="content_area">
		<!-- content -->
		<div id="content">
<?php get_sidebar();?>
			<!-- main -->
			<div id="main">
				<ul id="pnav" class="px11">
					<li><a href="<?php echo bloginfo('siteurl');?>/">HOME</a></li>
					<li>&gt; 「<?php echo esc_html($label);?>」の記事一覧</li>
				</ul>
				<div class="section_catalog">
					<h2 class="tit01"><img src="<?php echo bloginfo('siteurl');?>/images/headers/h2_<?php echo $post_type;?>_top_01.gif" alt="<?php echo esc_html($label);?>" /><span class="px18">の記事一覧</span></h2>
<?php if (have_posts()):?>
					<?php while(have_posts()): the_post();?>
					<div class="catalog_cont">
<?php if (!$noPic):?>
						<p class="catalog_pic"><a href="<?php the_permalink();?>" class="alpha"><?php echo get_the_post_thumbnail(get_the_ID(), array(200, 140));?></a></p>
<?php endif;?>
						<div class="catalog_inner<?php if ($noPic):?> catalog_inner_large<?php endif;?>">
							<p class="catalog_dates"><?php the_time('Y.m.d');?></p>
							<p class="catalog_tit px15"><a href="<?php the_permalink();?>"><?php the_title();?></a></p>
							<p class="catalog_txt"><?php echo do_shortcode('[custom_excerpt]');?></p>
							<p class="catalog_links"><a href="<?php the_permalink();?>">続きを読む&#187;</a></p>
						</div>
					</div>
<?php endwhile;?>

					<div class="pagelist px13">
<?php wp_pagenavi();?>
					</div>
<?php else:?>
					<p>投稿がありません</p>
<?php endif;?>
				</div>
			</div>
			<!-- /main -->
		</div>
		<!-- /content -->
	</div>
	<!-- /content_area -->
</div>
<!-- /wrap -->
<?php get_footer();?>
