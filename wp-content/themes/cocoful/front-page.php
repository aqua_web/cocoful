<?php
$footerAddText = '外出の参考にしてくださいね。';
?>
<?php get_header();?>
	<!-- content_area -->
	<div id="content_area">
		<!-- content -->
		<div id="content">
<?php get_sidebar();?>
			<!-- main -->
			<div id="main">
				<div id="mainimg">
					<div class="slider-wrapper theme-default">
						<div id="slider" class="nivoSlider">


<!--
<a href="present/index.html"><img src="images/pic_mainimg_03.jpg" width="650" height="270" alt="今月のプレゼント【6月・7月】" /></a>
-->
<a href="http://half-birthday.com/" target="_blank"><img src="images/haif_birthday.jpg" width="650" height="270" alt="Happy Half Birthday！" /></a>
<a href="about/index.html"><img src="images/pic_mainimg_02.jpg" width="650" height="270" alt="ココフルとは" /></a>

							
							
						</div>
					</div>
				</div>
				
				<div class="section_news">
					<div id="tabvanilla" class="menu1box box">
						<ul id="menu1" class="tabnav">
							<li><a href="#main1" class="li01"><span>新着情報</span></a></li>
							<li><a href="#main2" class="li02"><span>おでかけ</span></a></li>
							<li><a href="#main3" class="li03"><span>イベント・企画</span></a></li>
							<li><a href="#main4" class="li04"><span>コラム・連載</span></a></li>
							<li><a href="#main5" class="li05"><span>まなぶ・はたらく</span></a></li>
							<li class="last"><a href="#main6" class="li06"><span>お知らせ</span></a></li>
						</ul>
						<div id="main1" class="tabdiv">
<?php 
$icon = array(
	'outing',
	'event',
	'column',
	'working',
	'information'
);
$label = array(
	'outing' => 'おでかけ',
	'event' => 'イベント・企画',
	'column' => 'コラム・連載',
	'working' => 'まなぶ・はたらく',
	'information' => 'お知らせ'
);
?>
							<p class="news_tit"><img src="images/tit_news_01.gif" width="72" height="18" alt="新着情報" /></p>
<?php $posts = get_posts(array('post_type' => array('outing', 'event', 'column', 'working', 'information'), 'posts_per_page' => 6));?>
<?php if (count($posts)): foreach($posts as $p):?>
							<dl>
								<dt><?php echo get_the_time('Y/m/d', $p);?></dt>
								<dd class="news_img"><img src="images/txt_news_0<?php echo array_search(get_post_type($p), $icon) + 1;?>.gif" width="64" height="16" alt="<?php echo $label[get_post_type()];?>" /></dd>
								<dd class="news_txt"><a href="<?php echo get_permalink($p->ID);?>"><?php echo get_the_title($p);?></a></dd>
							</dl>
<?php endforeach; endif;?>
						</div>
<?php foreach ($icon as $ct => $posttype):?>
						<div id="main<?php echo $ct + 2;?>" class="tabdiv">
							<p class="news_tit"><img src="images/tit_news_0<?php echo $ct + 2;?>.gif" alt="<?php $label[$posttype];?>" /></p>
<?php wp_reset_query(); $posts = get_posts(array('post_type' => $posttype, 'posts_per_page' => 6));?>
<?php if (count($posts)): foreach($posts as $p):?>
							<dl>
								<dt><?php echo get_the_time('Y/m/d', $p);?></dt>
								<dd class="news_img"><img src="images/txt_news_0<?php echo $ct + 1;?>.gif" width="64" height="16" alt="<?php echo $label[get_post_type()];?>" /></dd>
								<dd class="news_txt"><a href="<?php echo get_permalink($p->ID);?>"><?php echo get_the_title($p);?></a></dd>
							</dl>
<?php endforeach; endif;?>
							</div>
<?php endforeach;?>
					</div>
				</div>
<!--
				<p class="main_btn"><a href="present/index.html" class="alpha"><img src="images/bnr_main_01.jpg" width="660" height="100" alt="今月のプレゼント【6月・7月】" /></a></p>
-->
				
<?php
$posts = get_posts(
	array(
		'post_type' => array('outing', 'event', 'column', 'working', 'information'),
		'posts_per_page' => 8,
		'meta_key' => 'recommend',
		'meta_value' => 1,
		'orderby' => 'post_date',
		'order' => 'DESC'
	)
);?>
<?php if (count($posts)):?>
				<div class="section_recommend">
					<p class="recommend_tit"><img src="images/tit_recommend_01.gif" width="212" height="25" alt="Recommend おすすめ情報" /></p>
					<ul>
<?php foreach($posts as $cnt => $p): $post_type = get_post_type($p); ?>
						<li class="heightLine<?php if ($cnt && $cnt % 4 == 3):?> mr0<?php endif;?>">
							<p class="recommend_category"><img src="images/txt_news_0<?php echo array_search($post_type, $icon) + 1;?>.gif" width="64" height="16" alt="<?php echo $label[$post_type];?>" /></p>
							<p class="recommend_pic"><a href="<?php echo get_permalink($p->ID);?>" class="alpha"><?php echo get_the_post_thumbnail($p->ID, array(145, 116), array('alt' => get_the_title($p)));?></a></p>
							<p class="recommend_date px11"><?php echo get_the_time('Y/m/d', $p);?></p>
							<p class="recommend_txt"><a href="<?php echo get_permalink($p->ID);?>"><?php echo get_the_title($p);?></a></p>
						</li>
<?php if ($cnt && $cnt % 4 == 3):?>
					</ul>
					<ul>
<?php endif;?>
<?php endforeach;?>
					</ul>
				</div>
<?php endif;?>
				
				<div class="section_link">
					<p class="link_tit"><img src="images/tit_link_01.gif" width="149" height="25" alt="Link おすすめサイト" /></p>

<ul>
						

					<ul>
<li><a href="http://mi-te.kumon.ne.jp/" target="_blank" class="alpha"><img src="images/bnr_link_04.jpg" width="212" height="60" alt="心スクスク！わが子に贈る絵本こそだて日記mi:te［ミーテ］" /></a></li>

<li><a href="http://www.kodomoboshi.com/" target="_blank" class="alpha"><img src="images/bnr_link_02.jpg" width="212" height="60" alt="東京こども星レストラン" /></a></li>
						<li class="pr0"><a href="http://ninps.com/" target="_blank" class="alpha"><img src="images/bnr_link_01.jpg" width="212" height="60" alt="ニンプス" /></a></li>
						
						<li><a href="http://www.child-rin.com/" target="_blank" class="alpha"><img src="images/bnr_link_03.jpg" width="212" height="60" alt="小さく、凛と、自由にNPO法人チルドリン" /></a></li>
					
						<li><a href="http://coconala.com/" target="_blank" class="alpha"><img src="images/bnr_link_05.jpg" width="212" height="60" alt="ココナラ" class="rollover" /></a></li>

						<li class="pr0"><a href="http://ameblo.jp/hughugchunakano/" target="_blank" class="alpha"><img src="images/bnr_link_06.jpg" width="212" height="60" alt="子育て&amp;女性応援「ハグハグチュー」" /></a></li>

						<li><a href="http://cheersmama.jp/" target="_blank" class="alpha"><img src="images/bnr_link_07.jpg" width="212" height="60" alt="Cheers! mama　[チアーズ ママ]　[チアママ]" </a></li>

<li><a href="http://babyframes.jp/" target="_blank" class="alpha"><img src="images/bnr_link_08.jpg" width="212" height="60" alt="ベビーフォトフレームのBaby Frames" </a></li>
					</ul>
				</div>
				
			</div>
			<!-- /main -->
		</div>
		<!-- /content -->
	</div>
	<!-- /content_area -->
</div>
<!-- /wrap -->
<?php get_footer();?>