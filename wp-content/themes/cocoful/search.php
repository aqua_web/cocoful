<?php
$post_type = $wp_query->get('post_type');
//$post_type = get_post_type();
switch ($post_type) {
	case "outing":
		$h1 = "ママ、主婦向けお出かけ情報一覧ページ【検索結果】"; break;
	case "column":
		$h1 = "コラム・連載一覧ページ【検索結果】"; break;
	case "event":
		$h1 = "イベント・企画一覧ページ【検索結果】"; break;
	case "working":
		$h1 = "はたく・まなぶ一覧ページ【検索結果】"; break;
	case "any":
		$h1 = "検索結果"; break;
}
$post_type_object = get_post_type_object($post_type);
if ($post_type != 'any') {
	$label = $post_type_object->labels->name;
} else {
	$label = 'ココフル';
}

get_header();
?>
	<!-- content_area -->
	<div id="content_area">
		<!-- content -->
		<div id="content">
<?php get_sidebar();?>
			<!-- main -->
			<div id="main">
				<ul id="pnav" class="px11">
					<li><a href="<?php echo bloginfo('siteurl');?>/">HOME</a></li>
<?php if ($post_type != 'any'):?>
					<li>&gt; <a href="<?php echo get_post_type_archive_link($post_type);?>/index.html"><?php echo $label;?></a></li>
<?php endif;?>
					<li>&gt; 検索結果 <?php echo the_search_query();?></li>
				</ul>
				<div class="section_catalog">
					<h2 class="tit01"><img src="<?php echo bloginfo('siteurl');?>/images/headers/h2_<?php echo $post_type;?>_top_01.gif" alt="<?php echo esc_html($label);?>" /><span class="px18">の記事一覧</span></h2>
					<div id="searchInfo">
						<h3>検索条件：<strong class="px16"><?php echo the_search_query();?></strong></h3>
						<p><strong class="px16"><?php echo $wp_query->found_posts;?></strong>件の記事が見つかりました</p>
					</div>
<?php while(have_posts()): the_post();?>
					<div class="catalog_cont">
						<p class="catalog_pic"><a href="<?php the_permalink();?>" class="alpha"><?php echo get_the_post_thumbnail(get_the_ID(), array(200, 140));?></a></p>
						<div class="catalog_inner">
							<p class="catalog_dates"><?php the_time('Y.m.d');?></p>
							<p class="catalog_tit px15"><a href="<?php the_permalink();?>"><?php the_title();?></a></p>
							<p class="catalog_txt"><?php echo do_shortcode('[custom_excerpt]');?></p>
							<p class="catalog_links"><a href="<?php the_permalink();?>">続きを読む&#187;</a></p>
						</div>
					</div>
<?php endwhile;?>

					<div class="pagelist px13">
<?php wp_pagenavi();?>
					</div>
				</div>
			</div>
			<!-- /main -->
		</div>
		<!-- /content -->
	</div>
	<!-- /content_area -->
</div>
<!-- /wrap -->
<?php get_footer();?>
