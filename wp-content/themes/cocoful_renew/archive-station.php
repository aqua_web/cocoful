<?php
$h1 = '駅構内情報一覧ページ';
$footerAddText = "外出の参考にして欲しい駅構内情報一覧ページ。";
if ($wp_query->get('line')) {
	$line = $wp_query->get('line');
	$lineObj = get_term_by('slug', $line, 'line');
	$label = $lineObj->name;
	$parent = "駅構内情報";
	$h1 = "駅沿線名一覧ページ";
	$footerAddText = "外出の参考にして欲しい駅沿線名一覧ページ。";
} else {
	$line = "";
}
get_header();
?>
<div id="content_area">
	<div class="container">
		<div class="row">
			<div class="col-md-8" id="col-main">

<?php if (!$line):?>
				<ul id="pnav" class="px11">
					<li><a href="<?php echo bloginfo('siteurl');?>/">HOME</a></li>
					<li>&gt; 駅構内情報</li>
				</ul>
				<div class="section_station">
					<h2 class="tit01"><img src="<?php echo bloginfo('siteurl');?>/images/headers/h2_station_top_01.gif" alt="駅構内情報" /></h2>
<?php
$company = array(
	'jr' => 'JR',
	'metro' => '東京メトロ',
	'tokyu' => '東急電鉄',
	'toei' => '都営地下鉄'
);
$ct = 0; 
foreach ($company as $slug => $name): $ct++;
?>
					<h3><img src="<?php echo bloginfo('siteurl');?>/images/headers/h3_station_top_<?php echo sprintf("%02d", $ct);?>.gif" alt="<?php echo esc_html($name);?>" /></h3>
					<ul>
<?php
$lines = get_terms('line', array('hide_empty' => 0, 'orderby' => 'order'));
	foreach ($lines as $line):
		if (preg_match("/^" . $slug . "_/", $line->slug)):
?>
						<li><a href="<?php echo str_replace("/line", "", get_category_link($line));?>/index.html"><?php echo esc_html($line->name);?></a></li>
<?php
		endif;
	endforeach;
?>
					</ul>
<?php 
endforeach;
?>
					</div>
<?php else:?>
				<ul id="pnav" class="px11">
					<li><a href="<?php echo bloginfo('siteurl');?>/">HOME</a></li>
					<li>&gt; <a href="../index.html">駅構内情報</a></li>
					<li>&gt; <?php echo esc_html($label);?></li>
				</ul>
				<div class="section_line">
					<h2 class="tit01"><img src="<?php echo bloginfo('siteurl');?>/images/headers/h2_station_<?php echo $line;?>_01.gif" alt="<?php echo esc_html($label);?>" /></h2>
<?php
query_posts($query_string . "&posts_per_page=-1");
?>
<?php if (have_posts()):?>
					<ul>
	<?php while(have_posts()): the_post();?>
						<li><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
	<?php endwhile;?>
					</ul>
<?php endif;?>
				</div>
<?php endif;?>
			</div>
			<div class="col-md-4" id="col-side">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>
