<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<title><?php wp_title(' | ', true, 'right');?>子連れでも安心しておでかけできる情報満載ポータルサイト　ココフル</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('siteurl');?>/common/css/fonts.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('siteurl');?>/common/css/module.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('siteurl');?>/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('siteurl');?>/common/css/meanmenu.css" media="all" />

<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('siteurl');?>/common/js/heightLine.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('siteurl');?>/common/js/common.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('siteurl');?>/common/js/jquery.meanmenu.min.js"></script>
<script>
$(document).ready(function() {
  $('#header_nav_mobile').meanmenu({
    meanScreenWidth: "992"
  });
});
</script>

<?php if  (is_front_page()):?>
<script type="text/javascript" src="<?php echo bloginfo('siteurl');?>/common/js/jquery.flexslider-min.js"></script>
<link rel="stylesheet" href="/common/css/flexslider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/common/css/easy-responsive-tabs.css">
<script src="/common/js/easy-responsive-tabs.js"></script>
<script type="text/javascript" charset="utf-8">
  $(window).load(function() {
    $('.flexslider').flexslider();
  });

$(function(){
  $("#tab-head li").addClass("tab1");
  $("#tab-head li a").eq(0).addClass("tab2");
  $("#tab-head li a").click(function() {
  $("#tab-body div").hide();
  $($(this).attr("href")).fadeIn();/*アニメーションで制御*/
  $("#tab-head li a").removeClass("tab2");
  $(this).addClass("tab2");
  return false;
});

$('#horizontalTab').easyResponsiveTabs({
  type: 'default', //Types: default, vertical, accordion           
  width: 'auto', //auto or any width like 600px
  fit: true,   // 100% fit in a container
  closed: 'accordion', // Start closed if in accordion view
  activate: function(event) { // Callback function if tab is switched
    var $tab = $(this);
    var $info = $('#tabInfo');
    var $name = $('span', $info);
    $name.text($tab.text());
    $info.show();
    }
});
$('#verticalTab').easyResponsiveTabs({
  type: 'vertical',
  width: 'auto',
  fit: true
});

});
</script>

<script type="text/javascript" src="common/js/jquery-ui-personalized-1.5.2.packed.js"></script>
<script type="text/javascript" src="common/js/sprinkle.js"></script>

<!-- ↓GA設定ここから↓ -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47207623-1', 'auto', {'allowLinker': true});
  ga('require', 'linker');
  ga('linker:autoLink', ['s360.jp'] );
  ga('send', 'pageview');

</script>
<!-- ↑GA設定ここまで↑ -->

<!--↓synergy tracking tagここから↓-->
<script type="text/javascript">
  <!--
  var __s360_tdatas = __s360_tdatas || [];
  (function() {
    __s360_tdatas.push(['track', 'MCwzMjE2NixTQw%3D%3D', null, null]);
    var code = document.createElement('script');
    code.type = 'text/javascript';
    code.async = true;
    code.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 's360.jp/t/t.js';
    var script = document.getElementsByTagName('script')[0];
    script.parentNode.insertBefore(code, script);
  })();
  // -->
</script>
<!--↑synergy tracking tagここまで↑-->


<?php endif;?>
<?php wp_head();?>
</head>
<body>
<!--↓ClickTale start tagここから↓-->
<!-- ClickTale Top part -->
<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->
<!--↑ClickTale start tagここまで↑-->

	<!-- header -->
<header>
  <div class="container">

    <h1 class="px11">
      <aside>子連れでも安心しておでかけできる情報満載ママ向けポータルサイト ココフル<?php echo $h1 ? "の" . esc_html($h1) : "";?></aside>
      <a href="<?php echo bloginfo('siteurl');?>/"><img src="<?php echo bloginfo('siteurl');?>/common/images/logo_01.gif" width="233" height="63" alt="Cocoful ココフル" /></a>
    </h1>

    <nav id="header_nav_mobile" class="hidden-md hidden-lg">
      <h1 id="hedding_mobile">
        <a href="<?php echo bloginfo('siteurl');?>/"><img src="<?php echo bloginfo('siteurl');?>/common/images/logo_01.gif" width="233" height="63" alt="Cocoful ココフル" /></a>
      </h1>
      <ul>
        <li><a href="/">ホーム</a></li>
 	      <li><a href="<?php echo bloginfo('siteurl');?>/outing/index.html">おでかけ</a></li>
 	      <li><a href="<?php echo bloginfo('siteurl');?>/event/index.html">イベント・企画</a></li>
 	      <li><a href="<?php echo bloginfo('siteurl');?>/column/index.html">コラム・連載</a></li>
 	      <li><a href="<?php echo bloginfo('siteurl');?>/working/index.html">まなぶ・はたらく</a></li>
 	      <li><a href="https://s360.jp/form/32166-13/" target="_blank">お問い合わせ</a></li>
      </ul>
    </nav>

    <div id="header_nav" class="clearfix visible-md visible-lg">
  		<ul class="px11">
	  		<li class="link_text"><a href="<?php echo bloginfo('siteurl');?>/company/index.html">会社概要</a></li>
		  	<li class="link_text"><a href="<?php echo bloginfo('siteurl');?>/sitemap/index.html">サイトマップ</a></li>
        <li><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fcocoful.jp%2F&amp;width=70&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=20" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:70px; height:20px;" allowTransparency="true"></iframe></li>
        <li><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://cocoful.jp/" data-lang="ja" data-count="none">ツイート</a>
		    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></li>
		  </ul>
      <div id="header_bnr visible-md visible-lg">
	  		<script type="text/JavaScript" src="<?php echo bloginfo('siteurl');?>/common/js/banner_header.js"></script>
        <script type="text/JavaScript">random_banner();</script>
      </div>
    </div>


    <ul id="nav" class="visible-md visible-lg">
<?php if (is_front_page()):?>
			<li><a href="<?php echo bloginfo('siteurl');?>/"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav01_on.jpg" width="159" height="35" alt="HOME" /></a></li>
<?php else:?>
			<li><a href="<?php echo bloginfo('siteurl');?>/"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav01_off.jpg" width="159" height="35" alt="HOME" /></a></li>
<?php endif;?>
<?php if ($post_type == 'outing'):?>
			<li><a href="<?php echo bloginfo('siteurl');?>/outing/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav02_on.jpg" width="156" height="35" alt="おでかけ" /></a></li>
<?php else:?>
			<li><a href="<?php echo bloginfo('siteurl');?>/outing/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav02_off.jpg" width="156" height="35" alt="おでかけ" /></a></li>
<?php endif;?>
<?php if ($post_type == 'event'):?>
			<li><a href="<?php echo bloginfo('siteurl');?>/event/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav03_on.jpg" width="157" height="35" alt="イベント・企画" /></a></li>
<?php else:?>
			<li><a href="<?php echo bloginfo('siteurl');?>/event/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav03_off.jpg" width="157" height="35" alt="イベント・企画" /></a></li>
<?php endif;?>
<?php if ($post_type == 'column'):?>
			<li><a href="<?php echo bloginfo('siteurl');?>/column/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav04_on.jpg" width="157" height="35" alt="コラム・連載" /></a></li>
<?php else:?>
			<li><a href="<?php echo bloginfo('siteurl');?>/column/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav04_off.jpg" width="157" height="35" alt="コラム・連載" /></a></li>
<?php endif;?>
<?php if ($post_type == 'working'):?>
			<li><a href="<?php echo bloginfo('siteurl');?>/working/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav05_on.jpg" width="157" height="35" alt="まなぶ・はたらく" /></a></li>
<?php else:?>
			<li><a href="<?php echo bloginfo('siteurl');?>/working/index.html"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav05_off.jpg" width="157" height="35" alt="まなぶ・はたらく" /></a></li>
<?php endif;?>
			<li><a href="https://s360.jp/form/32166-13/" target="_blank"><img src="<?php echo bloginfo('siteurl');?>/common/images/nav06_off.jpg" width="154" height="35" alt="お問い合わせ" /></a></li>
		</ul>
  
  </div>
</header>
	<!-- /header -->
