<?php
$post_type_object = get_post_type_object(get_post_type());
$label = $post_type_object->labels->name; //sprintf("ママ、主婦向け%s情報一覧", $post_type_object->labels->name);
$topicPathLabel = $post_type_object->labels->name;
$h1 = get_the_title() . "ページ";
$footerAddText = sprintf("外出の参考にして欲しい%s。", $h1);

if ($wp_query->get('column_category')) {
	$parent = $post_type_object->labels->name;
	$label_object = get_term_by('slug', $wp_query->get('column_category'), 'column_category');
	$label = $label_object->name;
}

if ($wp_query->get('working_category')) {
	$parent = $post_type_object->labels->name;
	$label_object = get_term_by('slug', $wp_query->get('working_category'), 'working_category');
	$label = $label_object->name;
}

$thisURL = urlencode(get_permalink($post));

the_post();
get_header();
?>
<div id="content_area">
	<div class="container">
		<div class="row">
			<div class="col-md-8" id="col-main">

				<ul id="pnav" class="px11">
					<li><a href="<?php echo bloginfo('siteurl');?>/">HOME</a></li>
<?php if (isset($parent)):?>
					<li>&gt; <a href="../index.html"><?php echo esc_html($parent);?></a></li>
					<li>&gt; <a href="index.html">「<?php echo esc_html($label);?>」の記事一覧</a></li>
					<li>&gt; <?php the_title();?></li>
<?php else:?>
					<li>&gt; <a href="index.html">「<?php echo esc_html($topicPathLabel);?>」の記事一覧</a></li>
					<li>&gt; <?php the_title();?></li>
<?php endif;?>
				</ul>
				<div class="section_catalogdetail">
					<h2 class="tit02"><span class="catalogdetail_stxt px16"><?php echo esc_html($label);?></span><span class="catalogdetail_sbtn"><iframe src="//www.facebook.com/plugins/like.php?href=<?php echo $thisURL;?>&amp;width=70&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=20" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:20px;" allowTransparency="true"></iframe></span><span class="catalogdetail_sbtn"><a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink();?>" data-lang="ja" data-count="none">ツイート</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></span></h2>
					<h3 class="px23"><?php the_title();?></h3>
					<h4><?php the_time('Y.m.d');?></h4>
					<div class="catalogdetail_cont">
<?php the_content();?>
					<ul>
							<li><iframe src="//www.facebook.com/plugins/like.php?href=<?php echo $thisURL;?>&amp;width=70&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=20" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:20px;" allowTransparency="true"></iframe></li>
							<li><a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink();?>" data-lang="ja" data-count="none">ツイート</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></li>
						</ul>
						<p class="catalogdetail_links px14">
						<?php echo previous_post_link('%link', '&laquo; 前の記事');?>
						<?php echo next_post_link('%link', '次の記事 &raquo;');?>
						</p>
					</div>

					<div class="related_tags">
						<p class="tags_tit px14">関連タグ</p>
<?php
$areas = get_the_terms($post->ID, 'area');
$generations = get_the_terms($post->ID, 'generation');
?>
						<ul>
<?php if (is_array($areas)): foreach ($areas as $t):?>
							<li><a href="<?php echo bloginfo('siteurl');?>/<?php echo get_post_type();?>/area/<?php echo esc_html($t->slug);?>"><?php echo esc_html($t->name);?></a></li>
<?php endforeach; endif;?>
<?php if (is_array($generations)): foreach ($generations as $t):?>
							<li><a href="<?php echo bloginfo('siteurl');?>/<?php echo get_post_type();?>/generation/<?php echo esc_html($t->slug);?>"><?php echo esc_html($t->name);?></a></li>
<?php endforeach; endif;?>
</ul>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="col-side">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>
