<?php
$h1 = "資格・はたらく";
$label = $h1;

if ($wp_query->get('working_category')) {
	/**
	 * 第２階層
	 */
	$term = get_term_by('slug', $wp_query->get('working_category'), 'working_category');
	$label = $term->name;
	$slug = $term->slug;
	$parent = $h1;
	$h1 = $term->name . '一覧ページ';
	$footerAddText = sprintf("外出の参考にして欲しい%s。", $h1);
	
} else {
	/**
	 * 第１階層
	 */
	$post_type = 'working';
	$category_term = 'working_category';
	$h1 .= "一覧ページ";
	$footerAddText = sprintf("外出の参考にして欲しい%s。", $h1);
	// 下層カテゴリ取得
	$sub_cats = get_terms('working_category', array('hide_empty' => 0, 'parent' => 0, 'orderby' => 'order'));
}
get_header();
?>
<div id="content_area">
	<div class="container">
		<div class="row">
			<div class="col-md-8" id="col-main">

<?php
if ($wp_query->get('working_category')) {
	include "include_archive_2.php";
} else {
	include "include_archive_1.php";
}
?>
			</div>
			<div class="col-md-4" id="col-side">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>
