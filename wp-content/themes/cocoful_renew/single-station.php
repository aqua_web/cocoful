<?php
$post_type_object = get_post_type_object(get_post_type());
$label = $post_type_object->labels->name;

$label_object = get_term_by('slug', $wp_query->get('line'), 'line');
$label = $label_object->name;
$parent = "駅構内情報";
$h1 = get_the_title() . "ページ";
$footerAddText = sprintf("外出の参考にして欲しい%s。", $h1);

$thisURL = urlencode(get_permalink($post));

the_post();
get_header();
?>
<div id="content_area">
	<div class="container">
		<div class="row">
			<div class="col-md-8" id="col-main">

				<ul id="pnav" class="px11">
					<li><a href="<?php echo bloginfo('siteurl');?>/">HOME</a></li>
					<li>&gt; <a href="../index.html">駅構内情報</a></li>
					<li>&gt; <a href="index.html"><?php echo esc_html($label);?></a></li>
					<li>&gt; <?php the_title();?></li>
				</ul>
				<div class="section_stationdetail">
					<h2 class="tit02"><span class="catalogdetail_stxt px16">駅構内情報</span><span class="catalogdetail_sbtn"><iframe src="//www.facebook.com/plugins/like.php?href=<?php echo $thisURL;?>&amp;width=70&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=20" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:20px;" allowTransparency="true"></iframe></span><span class="catalogdetail_sbtn"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://cocoful.jp/" data-lang="ja" data-count="none">ツイート</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></span></h2>
					<h3 class="px23"><?php echo esc_html($label);?> <?php the_title();?></h3>
					<div class="stationdetail_cont">
						<h4><img src="<?php echo bloginfo('siteurl');?>/images/headers/h4_station_detail_01.gif" width="128" height="25" alt="エレベーター情報" /></h4>
						<p><?php echo nl2br(post_custom('elevator'));?></p>
						<h4><img src="<?php echo bloginfo('siteurl');?>/images/headers/h4_station_detail_02.gif" width="143" height="25" alt="エスカレーター情報" /></h4>
						<p><?php echo nl2br(post_custom('escalator'));?></p>
						<h4><img src="<?php echo bloginfo('siteurl');?>/images/headers/h4_station_detail_03.gif" width="169" height="25" alt="トイレ・授乳・おむつ替え" /></h4>
						<p><?php echo nl2br(post_custom('toilet'));?></p>
						<h4><img src="<?php echo bloginfo('siteurl');?>/images/headers/h4_station_detail_04.gif" width="72" height="25" alt="周辺情報" /></h4>
						<p><?php echo nl2br(post_custom('area'));?></p>
					</div>
					<div class="stationdetail_cont2">
						<p><img src="<?php echo bloginfo('siteurl');?>/images/headers/tit_station_detail_toppic_01.gif" width="276" height="41" alt="ココフルからのワンポイント!" /></p>
						<p class="stationdetail_tit fo14"><?php echo nl2br(post_custom('onepoint_title'));?></p>
						<p class="stationdetail_txt px11"><?php echo nl2br(post_custom('onepoint_body'));?></p>
					</div>
					<ul class="stationdetail_btn">
						<li><iframe src="//www.facebook.com/plugins/like.php?href=<?php echo $thisURL;?>&amp;width=70&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=20" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:20px;" allowTransparency="true"></iframe></li>
						<li><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://cocoful.jp/" data-lang="ja" data-count="none">ツイート</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></li>
					</ul>
					<div class="related_tags">
						<p class="tags_tit px14">関連タグ</p>
<?php
$areas = get_the_terms($post->ID, 'area');
$generations = get_the_terms($post->ID, 'generation');
?>
						<ul>
<?php foreach ($areas as $t):?>
							<li><a href="<?php echo bloginfo('siteurl');?>/<?php echo get_post_type();?>/area/<?php echo esc_html($t->slug);?>"><?php echo esc_html($t->name);?></a></li>
<?php endforeach;?>
<?php foreach ($generations as $t):?>
							<li><a href="<?php echo bloginfo('siteurl');?>/<?php echo get_post_type();?>/generation/<?php echo esc_html($t->slug);?>"><?php echo esc_html($t->name);?></a></li>
<?php endforeach;?>
</ul>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="col-side">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>
