<?php 
/**
 * 2階層目のテンプレート
 */
?>
				<ul id="pnav" class="px11">
					<li><a href="<?php echo bloginfo('siteurl');?>/">HOME</a></li>
					<li>&gt; <a href="<?php echo get_post_type_archive_link($post_type)?>/index.html"><?php echo esc_html($parent);?></a></li>
					<li>&gt; 「<?php echo esc_html($term->name);?>」の記事一覧</li>
				</ul>
				<div class="section_catalog">
					<h2 class="tit01"><img src="<?php echo bloginfo('siteurl');?>/images/headers/tit_<?php echo $slug;?>_top_01.gif" alt="<?php echo esc_html($label);?>" /><span class="px18">の記事一覧</span></h2>
<?php if (have_posts()):?>
<?php while(have_posts()): the_post();?>
					<div class="catalog_cont">
						<div class="row">
							<div class="col-md-4">
								<p class="catalog_pic"><a href="<?php the_permalink();?>" class="alpha"><?php echo get_the_post_thumbnail(get_the_ID(), array(200, 140));?></a></p>
							</div>
							<div class="col-md-8">
								<div class="catalog_inner">
									<p class="catalog_dates"><?php the_time('Y.m.d');?></p>
									<p class="catalog_tit px15"><a href="<?php the_permalink();?>"><?php the_title();?></a></p>
									<p class="catalog_txt"><?php echo do_shortcode('[custom_excerpt]');?></p>
									<p class="catalog_links"><a href="<?php the_permalink();?>">続きを読む&#187;</a></p>
								</div>
							</div>
						</div>
					</div>
<?php endwhile;?>

					<div class="pagelist px13">
<?php wp_pagenavi();?>
					</div>
<?php else:?>
					<p>投稿がありません</p>
<?php endif;?>
				</div>
