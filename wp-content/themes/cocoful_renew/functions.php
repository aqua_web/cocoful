<?php
$_SERVER['REQUEST_URI'] = str_replace( '/index.html', '', $_SERVER['REQUEST_URI'] );

/**
 * カスタム投稿タイプ
 */
add_action('init', 'cf_custom_init');
function cf_custom_init() {
	// おでかけ
	$typename = 'outing';
	$labels = array(
		'name' => _x('おでかけ', 'post type general name'),
		'singular_name' => _x('おでかけ', 'post type singular name'),
		'add_new' => _x('新しく記事を書く', $typename),
		'add_new_item' => __('おでかけ記事を書く'),
		'edit_item' => __('おでかけ記事を編集'),
		'new_item' => __('新しいおでかけ記事'),
		'view_item' => __('おでかけ記事を見る'),
		'search_items' => __('おでかけ記事を探す'),
		'not_found' =>  __('おでかけ記事はありません'),
		'not_found_in_trash' => __('ゴミ箱におでかけ記事はありません'),
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => false,
		'rewrite' => array('slug' => 'outing'),
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title', 'editor', 'author', 'revisions', 'thumbnail'),
		'has_archive' => true
	);
	register_post_type($typename, $args);
	
	// イベント・企画
	$typename = 'event';
	$labels = array(
		'name' => _x('イベント・企画', 'post type general name'),
		'singular_name' => _x('イベント・企画', 'post type singular name'),
		'add_new' => _x('新しく記事を書く', $typename),
		'add_new_item' => __('イベント・企画記事を書く'),
		'edit_item' => __('イベント・企画記事を編集'),
		'new_item' => __('新しいイベント・企画記事'),
		'view_item' => __('イベント・企画記事を見る'),
		'search_items' => __('イベント・企画記事を探す'),
		'not_found' =>  __('イベント・企画記事はありません'),
		'not_found_in_trash' => __('ゴミ箱にイベント・企画記事はありません'),
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 6,
		'supports' => array('title', 'editor', 'author', 'revisions', 'thumbnail'),
		'has_archive' => true
	);
	register_post_type($typename, $args);


	// コラム・連載
	$typename = 'column';
	$labels = array(
		'name' => _x('コラム・連載', 'post type general name'),
		'singular_name' => _x('コラム・連載', 'post type singular name'),
		'add_new' => _x('新しく記事を書く', $typename),
		'add_new_item' => __('コラム・連載記事を書く'),
		'edit_item' => __('コラム・連載記事を編集'),
		'new_item' => __('新しいコラム・連載記事'),
		'view_item' => __('コラム・連載記事を見る'),
		'search_items' => __('コラム・連載記事を探す'),
		'not_found' =>  __('コラム・連載記事はありません'),
		'not_found_in_trash' => __('ゴミ箱にコラム・連載記事はありません'),
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 7,
		'supports' => array('title', 'editor', 'author', 'revisions', 'thumbnail'),
		'has_archive' => true
	);
	register_post_type($typename, $args);

	// まなぶ・はたらく
	$typename = 'working';
	$labels = array(
		'name' => _x('まなぶ・はたらく', 'post type general name'),
		'singular_name' => _x('まなぶ・はたらく', 'post type singular name'),
		'add_new' => _x('新しく記事を書く', $typename),
		'add_new_item' => __('まなぶ・はたらく記事を書く'),
		'edit_item' => __('まなぶ・はたらく記事を編集'),
		'new_item' => __('新しいまなぶ・はたらく記事'),
		'view_item' => __('まなぶ・はたらく記事を見る'),
		'search_items' => __('まなぶ・はたらく記事を探す'),
		'not_found' =>  __('まなぶ・はたらく記事はありません'),
		'not_found_in_trash' => __('ゴミ箱にまなぶ・はたらく記事はありません'),
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 8,
		'supports' => array('title', 'editor', 'author', 'revisions', 'thumbnail'),
		'has_archive' => true
	);
	register_post_type($typename, $args);
	
	// 新着情報
	$typename = 'information';
	$labels = array(
		'name' => _x('お知らせ', 'post type general name'),
		'singular_name' => _x('お知らせ', 'post type singular name'),
		'add_new' => _x('新しく記事を書く', $typename),
		'add_new_item' => __('お知らせ記事を書く'),
		'edit_item' => __('お知らせ記事を編集'),
		'new_item' => __('新しいお知らせ記事'),
		'view_item' => __('お知らせ記事を見る'),
		'search_items' => __('お知らせ記事を探す'),
		'not_found' =>  __('お知らせ記事はありません'),
		'not_found_in_trash' => __('ゴミ箱にお知らせ記事はありません'),
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title', 'editor', 'author', 'revisions'),
		'has_archive' => true
	);
	register_post_type($typename, $args);
	
	// 駅構内情報
	$typename = 'station';
	$labels = array(
		'name' => _x('駅構内情報', 'post type general name'),
		'singular_name' => _x('駅構内情報', 'post type singular name'),
		'add_new' => _x('新しく記事を書く', $typename),
		'add_new_item' => __('駅構内情報記事を書く'),
		'edit_item' => __('駅構内情報記事を編集'),
		'new_item' => __('新しい駅構内情報記事'),
		'view_item' => __('駅構内情報記事を見る'),
		'search_items' => __('駅構内情報記事を探す'),
		'not_found' =>  __('駅構内情報記事はありません'),
		'not_found_in_trash' => __('ゴミ箱に駅構内情報記事はありません'),
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title', 'editor', 'author', 'revisions'),
		'has_archive' => true
	);
	register_post_type($typename, $args);
	
	// 特集
	$typename = 'special';
	$labels = array(
		'name' => _x('特集', 'post type general name'),
		'singular_name' => _x('特集', 'post type singular name'),
		'add_new' => _x('新しく記事を書く', $typename),
		'add_new_item' => __('特集記事を書く'),
		'edit_item' => __('特集記事を編集'),
		'new_item' => __('新しい特集記事'),
		'view_item' => __('特集記事を見る'),
		'search_items' => __('特集記事を探す'),
		'not_found' =>  __('特集記事はありません'),
		'not_found_in_trash' => __('ゴミ箱に特集記事はありません'),
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title', 'editor', 'author', 'revisions'),
		'has_archive' => true
	);
	register_post_type($typename, $args);

	// editor
	add_editor_style("style.css");
	
	// head内の余計なタグを削除
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'feed_links_extra', 3);
}

/**
 * カスタム分類
 */
add_action('init', 'cf_custom_taxonomy');
function cf_custom_taxonomy() {
	// 地域
	$labels = array(
		'name' => '地域',
		'singular_name' => '地域',
		'search_items' => '地域を探す',
		'all_items' => '地域名一覧',
		'parent_item' => '親となる地域',
		'parent_item_colon' => '親となる地域',
		'edit_item' => '地域名を編集',
		'update_item' => '地域名を更新',
		'add_new_item' => '地域を追加する'
	);
	register_taxonomy(
		'area',
		array('outing', 'event', 'column', 'working', 'special', 'station', 'information'),
		array(
			'labels' => $labels,
			'hierarchical' => true,
			'sort' => true,
			'show_ui' => true,
			'query_var' => true,
			'args' => array('orderby' => 'term_order'),
			'rewrite' => array('slug' => 'area'),
		)
	);
	
	// 年齢別
	$labels = array(
		'name' => '年齢別',
		'singular_name' => '年齢別',
		'search_items' => '年代を探す',
		'all_items' => '年代',
		'edit_item' => '年代を編集',
		'update_item' => '年代を更新',
		'add_new_item' => '年代を追加する'
	);
	register_taxonomy(
		'generation',
		array('outing', 'event', 'column', 'working', 'special', 'station', 'information'),
		array(
			'labels' => $labels,
			'hierarchical' => true,
			'sort' => true,
			'show_ui' => true,
			'query_var' => true,
			'args' => array('orderby' => 'term_order'),
			'rewrite' => array('slug' => 'generation'),
		)
	);
	
	// コラム・連載用カテゴリ
	$labels = array(
		'name' => 'コラム・連載カテゴリー',
		'singular_name' => 'カテゴリー',
		'search_items' => '年代を探す',
		'all_items' => 'カテゴリー',
		'edit_item' => 'カテゴリーを編集',
		'update_item' => 'カテゴリーを更新',
		'add_new_item' => 'カテゴリーを追加する'
	);
	register_taxonomy(
		'column_category',
		array('column'),
		array(
			'labels' => $labels,
			'hierarchical' => true,
			'sort' => true,
			'show_ui' => true,
			'query_var' => true,
			'args' => array('orderby' => 'term_order'),
			'rewrite' => array('slug' => 'column_category'),
		)
	);

	// まなぶ・はたらく用カテゴリ
	$labels = array(
		'name' => 'まなぶ・はたらくカテゴリー',
		'singular_name' => 'カテゴリー',
		'search_items' => '年代を探す',
		'all_items' => 'カテゴリー',
		'edit_item' => 'カテゴリーを編集',
		'update_item' => 'カテゴリーを更新',
		'add_new_item' => 'カテゴリーを追加する'
	);
	register_taxonomy(
		'working_category',
		array('working'),
		array(
			'labels' => $labels,
			'hierarchical' => true,
			'sort' => true,
			'show_ui' => true,
			'query_var' => true,
			'args' => array('orderby' => 'term_order'),
			'rewrite' => array('slug' => 'working_category'),
		)
	);

	// 路線
	$labels = array(
		'name' => '路線',
		'singular_name' => '路線',
		'search_items' => '路線を探す',
		'all_items' => '路線',
		'edit_item' => '路線を編集',
		'update_item' => '路線を更新',
		'add_new_item' => '路線を追加する'
	);
	register_taxonomy(
		'line',
		array('station'),
		array(
			'labels' => $labels,
			'hierarchical' => true,
			'sort' => true,
			'show_ui' => true,
			'query_var' => true,
			'args' => array('orderby' => 'term_order'),
			'rewrite' => array('slug' => 'line'),
		)
	);
}


/**
 * テーマ
 */
add_action('after_setup_theme', 'cf_theme_setup');
function cf_theme_setup() {
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size(200, 140);
}



/**
 * 管理画面内の不要メニューを非表示
 */
add_action('admin_menu', 'cf_remove_menus');
function cf_remove_menus() {
	global $menu;
	
	// 「メディア」の位置を下げる
	$menu[15] = $menu[10];
	unset($menu[10]);
	$menu[14] = $menu[4];
	
	// 非表示
	unset($menu[5]); // 投稿
//	unset($menu[20]); // ページ
	unset($menu[25]); // コメント
	if (!current_user_can('level_10')) {
		unset($menu[60]); // 外観
		unset($menu[65]); // プラグイン
		unset($menu[75]); // ツール
		unset($menu[80]); // 設定
	}
}


/**
 * エディタのカスタマイズ
 */
//カスタム投稿編集画面にAddQuicktagを追加
function addquicktag_set_custom_post_type($post_types){
	unset($post_types[0]);	// post削除
//	unset($post_types[1]);	// page削除
	unset($post_types[2]);	// comment削除
	unset($post_types[3]);	// edit-comments削除
	
    array_push($post_types, 'outing', 'event', 'column', 'working', 'special', 'information', 'station');
    return $post_types;
}
add_filter('addquicktag_post_types', 'addquicktag_set_custom_post_type');


//
add_filter( 'mce_buttons_2', 'cf_mce_editor_buttons' );
function cf_mce_editor_buttons( $buttons ) {

    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

function cf_custom_tinymce($initArray) {
	//選択できるブロック要素を変更
	$initArray['theme_advanced_blockformats'] = 'p,h2,h3,h4,h5,dt,dd,div';
	
	$style_formats = array(
		array(
		'title' => '写真のコメント',
			'block' => 'p',
			'classes' => 'pic-note'
		),
	);
	$initArray['style_formats'] = json_encode($style_formats);
	
	return $initArray;
}
add_filter('tiny_mce_before_init', 'cf_custom_tinymce');


/**
 * ショートコード
 */
function cf_custom_excerpt() {
	global $post;
	$body = strip_tags(get_the_content());
	return mb_substr($body, 0, 120) . (mb_strlen($body) > 120 ? '...' : '');
}
add_shortcode('custom_excerpt', 'cf_custom_excerpt');


/**
 *  検索
 */
function cf_custom_query_vars($public_query_vars) {
	$public_query_vars[] = 'area';
	$public_query_vars[] = 'generation';
	return $public_query_vars;
}

function cf_custom_parse_query($query) {
    if ( $query->get('area') || $query->get('generation')) {
        $query->is_search = true;
        $query->is_home = false;
    }
    return $query;
}
function cf_custom_get_search_query($str) {
	global $wp_query;
	if ($str) {
		$str = sprintf("「キーワード：%s」", $str);
	}
	if ($wp_query->get('area')) {
		$str .= sprintf("「地域： %s」", get_term_by('slug', $wp_query->get('area'), 'area')->name);
	}
	if ($wp_query->get('generation')) {
		$str .= sprintf("「年代： %s」", get_term_by('slug', $wp_query->get('generation'), 'generation')->name);
	}
	return $str;
}
function cf_posts_orderby($orderby) {
	$orderby = 'FIELD(post_type, "station") ASC, post_date DESC, post_type';
	return $orderby;
}

add_filter('query_vars', 'cf_custom_query_vars');
add_filter('parse_query', 'cf_custom_parse_query');
add_filter('get_search_query', 'cf_custom_get_search_query');
add_filter('posts_orderby', 'cf_posts_orderby');


/**
 * 地域検索
 */
add_filter('wp_list_categories', 'cf_custom_wp_list_categories');
function cf_custom_wp_list_categories($output) {
	global $wp_query;
	$post_type = $wp_query->get('post_type');
	$rep =  $post_type && $post_type != 'any' ? $post_type . "/" : "";
	$output = preg_replace("/<(\/?)li/", "<$1p", $output);
	$output = preg_replace("|/outing/|", "/" . $rep, $output);
	return $output;
}


/**
 * タイトルタグ
 */
add_filter('wp_title', 'cf_custom_wp_title');
function cf_custom_wp_title($title) {
	global $wp_query;
	if (is_search()) {
		$post_type_object = get_post_type_object($wp_query->get('post_type'));
		if ($post_type_object->labels->name) {
			$title = sprintf("検索結果 %s | %s |", get_search_query(), $post_type_object->labels->name);
		} else {
			$title = sprintf("検索結果 %s | ", get_search_query());
		}
	}
	
	global $label, $parent, $h1;
	if (is_archive()) {
		if (isset($label) && isset($parent)) {
			if ($wp_query->get('line')) {
				$title = sprintf("%s | %s | ", $label, $parent);
			} else {
				$title = sprintf("%s | %s | ", $label, $parent);
			}
		} elseif (isset($h1)) {
			$title = sprintf("%s |", str_replace("ページ", "", $h1));
		}
	}
	
	if (is_single()) {
		global $post;
		if (isset($parent)) {
			$title = sprintf("%s | %s | %s | ", get_the_title($post), $label, $parent);
		} else {
			$title = sprintf("%s | %s | ", get_the_title($post), $label);
		}
	}
	return $title;
}


/**
 * 2階層目のパーマリンク変更
 */
add_action('init', 'cf_flush_rewrite_rules');
//add_action('generate_rewrite_rules', 'cf_add_rewrite_rules');
function cf_flush_rewrite_rules() {
	global $wp_rewrite;
	
	$terms = get_terms('column_category', array('hide_empty' => 0));
	foreach ($terms as $term) {
		add_rewrite_rule('column/(' . $term->slug . ')/entry_([0-9]+)\.html?', 'index.php?post_type=column&column_category=$matches[1]&p=$matches[2]', 'top');
		add_rewrite_rule('column/(' . $term->slug . ')/page/([0-9]+)', 'index.php?post_type=column&column_category=$matches[1]&paged=$matches[2]', 'top');
		add_rewrite_rule('column/(' . $term->slug . ')/?', 'index.php?post_type=column&column_category=$matches[1]', 'top');
	}

	$terms = get_terms('working_category', array('hide_empty' => 0));
	foreach ($terms as $term) {
		add_rewrite_rule('working/(' . $term->slug . ')/entry_([0-9]+)\.html?', 'index.php?post_type=working&working_category=$matches[1]&p=$matches[2]', 'top');
		add_rewrite_rule('working/(' . $term->slug . ')/page/([0-9]+)', 'index.php?post_type=working&working_category=$matches[1]&paged=$matches[2]', 'top');
		add_rewrite_rule('working/(' . $term->slug . ')/?', 'index.php?post_type=working&working_category=$matches[1]', 'top');

		add_rewrite_rule('manabu/(' . $term->slug . ')/entry_([0-9]+)\.html?', 'index.php?post_type=working&working_category=$matches[1]&p=$matches[2]', 'top');
		add_rewrite_rule('manabu/(' . $term->slug . ')/page/([0-9]+)', 'index.php?post_type=working&working_category=$matches[1]&paged=$matches[2]', 'top');
		add_rewrite_rule('manabu/(' . $term->slug . ')/?', 'index.php?post_type=working&working_category=$matches[1]', 'top');
	}

	$terms = get_terms('line', array('hide_empty' => 0));
	foreach ($terms as $term) {
		add_rewrite_rule('station/(' . $term->slug . ')/([a-z_0-9\-]+)\.html?', 'index.php?post_type=station&line=$matches[1]&name=$matches[2]', 'top');
		add_rewrite_rule('station/(' . $term->slug . ')/page/([0-9]+)', 'index.php?post_type=station&line=$matches[1]&paged=$matches[2]', 'top');
		add_rewrite_rule('station/(' . $term->slug . ')/?', 'index.php?post_type=station&line=$matches[1]', 'top');
	}
	
	//$wp_rewrite->flush_rules();
}


/**
 * ダッシュボードカスタマイズ
 */
add_filter('dashboard_glance_items', 'cf_dashboard_glance_items');
function cf_dashboard_glance_items($elements) {
	foreach (array('outing', 'event', 'column', 'working', 'information', 'special', 'station') as $post_type) {
		$post_type_obj = get_post_type_object($post_type);
		$num_posts = wp_count_posts( $post_type );
		$elements[] = sprintf('<a href="edit.php?post_type=%1$s" class="%1$s-count">%3$s: %2$s件の投稿</a>', $post_type, number_format_i18n($num_posts->publish), $post_type_obj->label);
	}
	return $elements;
}
add_action('wp_dashboard_setup', 'cf_wp_dashboard_setup');
function cf_wp_dashboard_setup() {
	if (!current_user_can('level_10')) { //level10以下のユーザーの場合ウィジェットをunsetする
		global $wp_meta_boxes;
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']); // アクティビティ
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressニュース
	}
}


/**
 * 一覧画面にRecommend掲載有無を表示
 */
add_filter( 'manage_posts_columns', 'cf_manage_posts_columns' );
add_action( 'manage_posts_custom_column', 'cf_add_column', 10, 2 );
function cf_manage_posts_columns($columns) {
	global $post_type;
	if ($post_type != 'station') {
		$date = $columns['date'];
		unset($columns['date']);
		$columns['recommend'] = "Recommend掲載";
		$columns['date'] = $date;
	}
	return $columns;
}
function cf_add_column($column_name, $post_id) {
	if( $column_name == 'recommend' ) {
		$flag = get_field('recommend', $post_id);
		if ($flag) {
			echo "○";
		}
	}
}
