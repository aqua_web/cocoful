<?php
get_header();
?>
	<!-- content_area -->
	<div id="content_area">
		<!-- content -->
		<div id="content">
<?php get_sidebar();?>
			<!-- main -->
			<div id="main">
				<p style="font-size:1.3em;margin-bottom:20px;">ページが見つかりませんでした。</p>
				<p><a href="<?php bloginfo('siteurl');?>">トップページ</a>に戻り、再度アクセスしてください</p>
			</div>
			<!-- /main -->
		</div>
		<!-- /content -->
	</div>
	<!-- /content_area -->
</div>
<!-- /wrap -->
<?php get_footer();?>
