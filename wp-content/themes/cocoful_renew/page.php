<?php
the_post();
get_header();
?>
<div id="content_area">
	<div class="container">
		<div class="row">
			<div class="col-md-8" id="col-main">

				<ul id="pnav" class="px11">
					<li><a href="<?php echo bloginfo('siteurl');?>/">HOME</a></li>
					<li>&gt; <?php the_title();?></li>
				</ul>

				<div class="page_content">
					<?php the_content();?>
				</div>
			</div>
			<div class="col-md-4" id="col-side">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>
