<?php
$footerAddText = '外出の参考にしてくださいね。';
?>
<?php get_header();?>
<div id="content_area">
	<div class="container">
		<div class="row">
			<div class="col-md-8" id="col-main">
				<div id="mainimg">
					<div class="slider-wrapper theme-default">
						<div id="slider" class="flexslider">
							<ul class="slides">
								<li>
									<a href="present/index.html"><img src="images/pic_mainimg_03.jpg" alt="今月のプレゼント" /></a>
								</li>
								<li>
									<a href="http://half-birthday.com/" target="_blank"><img src="images/haif_birthday.jpg" alt="Happy Half Birthday！" /></a>
								</li>
								<li>
									<a href="about/index.html"><img src="images/pic_mainimg_02.jpg" width="650" height="270" alt="ココフルとは" /></a>
								</li>
							</ul>						
						</div>
					</div>
				</div>
				
				<div id="horizontalTab">
					<ul class="resp-tabs-list">
						<li>新着情報</li>
						<li>おでかけ</li>
						<li>イベント・企画</li>
						<li>コラム・連載</li>
						<li>まなぶ・はたらく</li>
						<li>お知らせ</li>
					</ul>

					<div class="resp-tabs-container">
						<div class="resp-tab-content">
<?php
$icon  = array(
    'outing',
    'event',
    'column',
    'working',
    'information'
);
$label = array(
    'outing' => 'おでかけ',
    'event' => 'イベント・企画',
    'column' => 'コラム・連載',
    'working' => 'まなぶ・はたらく',
    'information' => 'お知らせ'
);
?>
							<p class="news_tit"><img src="images/tit_news_01.gif" width="72" height="18" alt="新着情報" /></p>


							<ul>
<?php
$posts = get_posts(array(
    'post_type' => array(
        'outing',
        'event',
        'column',
        'working',
        'information'
    ),
    'posts_per_page' => 6
));

$posts = get_posts(array('post_type' => array('outing', 'event', 'column', 'working', 'information'), 'posts_per_page' => 6));

if (count($posts)):
    foreach ($posts as $p):
?>
							<li class="clearfix">
								<div class="pull-left" style="width:150px;">
								<span class="date"><?php echo get_the_time('Y/m/d', $p);
?></span>
								<span class="news_img"><img src="images/txt_news_0<?php echo array_search(get_post_type($p), $icon) + 1;?>.gif" width="64" height="16" alt="<?php echo $label[get_post_type()];?>" /></span>
								</div>
								<div class="news_title">
									<a href="<?php echo get_permalink($p->ID);
?>"><?php echo get_the_title($p);
?></a>
								</div>
							</li>
<?php
    endforeach;
endif;
?>
							</ul>
						</div>
<?php
foreach ($icon as $ct => $posttype):
?>
						<div class="resp-tab-content">
							<p class="news_tit"><img src="images/tit_news_0<?php echo $ct + 2;
?>.gif" alt="<?php $label[$posttype];
?>" /></p>
							<ul>
<?php
    wp_reset_query();
$posts = get_posts(array(
        'post_type' => $posttype,
        'posts_per_page' => 6
    ));
?>
<?php
    if (count($posts)):
        foreach ($posts as $p):
?>
							<li class="clearfix">
								<div class="pull-left" style="width:150px;">
									<span class="date"><?php echo get_the_time('Y/m/d', $p);
?></span>
									<span class="news_img"><img src="images/txt_news_0<?php echo $ct + 1;?>.gif" width="64" height="16" alt="<?php echo $label[get_post_type()];?>" /></span>
								</div>
								<div class="news_title">
									<a href="<?php echo get_permalink($p->ID);?>"><?php echo get_the_title($p);?></a>
								</div>
							</li>
<?php
        endforeach;
endif;
?>
							</ul>
						</div>
<?php
endforeach;
?>

				</div>
			</div>

<?php
$posts = get_posts(array(
    'post_type' => array(
        'outing',
        'event',
        'column',
        'working',
        'information'
    ),
    'posts_per_page' => 8,
    'meta_key' => 'recommend',
    'meta_value' => 1,
    'orderby' => 'post_date',
    'order' => 'DESC'
));
?>
<?php
if (count($posts)):
?>
				<div class="section_recommend">
					<p class="recommend_tit"><img src="images/tit_recommend_01.gif" width="212" height="25" alt="Recommend おすすめ情報" /></p>
					<div class="posts row">
					
<?php
    foreach ($posts as $cnt => $p):
        $post_type = get_post_type($p);
?>
						<div class="post col-sm-3 col-xs-6">
							<div class="heightLine">
							<p class="recommend_category"><img src="images/txt_news_0<?php echo array_search($post_type, $icon) + 1;?>.gif" width="64" height="16" alt="<?php echo $label[$post_type];?>" /></p>
							<p class="recommend_pic"><a href="<?php echo get_permalink($p->ID);?>" class="alpha"><?php
        echo get_the_post_thumbnail($p->ID, array(
            145,
            116
        ), array(
            'alt' => get_the_title($p)
        ));
?></a></p>
							<p class="recommend_date px11"><?php
        echo get_the_time('Y/m/d', $p);
?></p>
							<p class="recommend_txt"><a href="<?php
        echo get_permalink($p->ID);
?>"><?php
        echo get_the_title($p);
?></a></p>
						</div>
						</div>
<?php
    endforeach;
?>
					</div>
				</div>
<?php
endif;
?>

				<div class="section_link visible-md visible-lg">
					<?php 
						// リンクバナー
						include(TEMPLATEPATH .'/section_link.php'); 
					?>
				</div>

			</div>
			<div class="col-md-4" id="col-side">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>